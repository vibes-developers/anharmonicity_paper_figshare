import click
import numpy as np
import xarray as xr

from plot_config import config, fontsize, plt
from vibes.anharmonicity_score import get_sigma_per_mode

config.subplots_adjust.kw.update(
    {"left": 0.15, "right": 0.97, "bottom": 0.2, "top": 0.9, "wspace": 0.15}
)

c1 = config.color.c1k
c2 = config.color.c2k

ds1 = xr.load_dataset("../datasets/Si.nc")
ds2 = xr.load_dataset("../datasets/KCaF3.nc")


@click.command()
@click.argument("absolute", default=False)
def plot(absolute):
    s1 = get_sigma_per_mode(ds1, absolute=absolute)
    s2 = get_sigma_per_mode(ds2, absolute=absolute)

    plot_kw = {"lw": 0, "alpha": 0.5}

    # fig, (ax1, ax2) = plt.subplots(ncols=2, sharey=True)
    fig, ax = plt.subplots()
    fig.subplots_adjust(**config.subplots_adjust.kw)
    fig.set_size_inches(config.size.width, config.size.height)

    ax.plot(s1.iloc[3:], **plot_kw, c=c1, mfc=c1, mew=0, marker="o", ms=3.5)
    ax.plot(s2.iloc[3:], **plot_kw, c=c2, mfc=c2, mew=0, marker="s", ms=3.5)

    kw = {"window": 50, "center": True, "min_periods": 10}
    ax.plot(s1.iloc[3:].rolling(**kw).mean(), c="k")
    ax.plot(s2.iloc[3:].rolling(**kw).mean(), c="k")

    # plot room temperature
    # 300K -> 6.25098529084 THz
    x_300K = 6.25098529084
    ax.axvline(x_300K, ls="--", c="k", zorder=0)

    # ax.legend(("Si", ), fancybox=False)
    # Si
    kw = {
        "ha": "center",
        "va": "center",
        "transform": fig.transFigure,
        "fontsize": fontsize + 2,
    }

    # ticks
    ax.set_xticks(np.arange(0, 16, 5))
    ax.set_xticks(np.arange(0, 16, 1), minor=True)
    ax.set_yticks(np.arange(0, 1.1, 0.2))
    ax.set_yticks(np.arange(0, 1.2, 0.1), minor=True)
    ax.tick_params(direction="in", which="both", right=True, top=True)

    ax.set_xlabel(r"$\omega_{s}$ (THz)")

    if not absolute:
        ax.set_xlim(0, 16)
        ax.set_ylim(0, 1.2)

        # ax.set_ylabel(r"$\sigma [ F^\mathrm{A}_s ]$ / $\sigma [ F_s ]$")
        # ax.set_ylabel(r"$\sigma^\mathrm{A}_s$", rotation=0, labelpad=10)
        ax.set_ylabel(r"$\sigma^\mathrm{A}_s (300\,\mathrm{K})$", labelpad=2)
        ax.text(0.24, 0.275, "Si", rotation=0, color=c1, **kw)
        ax.text(0.24, 0.75, r"KCaF$_3$", rotation=0, color=c2, **kw)
        kw["fontsize"] = fontsize
        # ax.plot([x_300K, x_300K * 1.05], 2 * [1.1,], c='k')
        # s = r"$300\,\mathrm{K} \cdot k_{\rm B} / \hbar$"
        # plt.text(
        #     0.59, 0.83, s, rotation=0, color="k", **kw,
        # )

        figname = "sigma_mode.pdf"
        fig.savefig(figname, **config.save.kw)
        print(f"Plot saved to {figname}")
    else:
        ax.set_xlim(0, 16)
        ax.set_ylim(0, 0.6)

        ax.set_ylabel(r"$\sigma [ F^\mathrm{A}_s ]$")
        plt.text(0.8, 0.3, "Si", rotation=10, color=c1, **kw)
        plt.text(0.775, 0.625, r"KCaF$_3$", rotation=15, color=c2, **kw)

        figname = "sigma_mode_absolute.pdf"
        fig.savefig(figname, **config.save.kw)
        print(f"Plot saved to {figname}")


if __name__ == "__main__":
    plot()
