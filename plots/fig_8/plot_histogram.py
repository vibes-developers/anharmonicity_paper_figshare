"""inspired by https://www.bastibl.net/publication-quality-plots/"""
import numpy as np
import pandas as pd
import xarray as xr
from scipy import stats

from plot_config import config, fontsize, plt
from utils import get_forces_mode

# adjust height
config.size.height *= 0.875

log = open("histogram.log", "w")


ds1 = xr.load_dataset("../datasets/Si.nc")
ds2 = xr.load_dataset("../datasets/KCaF3.nc")


fig, (ax1, ax2) = plt.subplots(ncols=2, sharey=True)
fig.subplots_adjust(**config.subplots_adjust.kw)
fig.set_size_inches(config.size.width, config.size.height)


npoints = 35j
xlim, ylim = 1, 1


def get_xy(ds, s=0):
    """return f and fA as pd.Series"""
    f, fh, _ = get_forces_mode(ds)
    std = f.std()

    x = f[:, s].flatten() / std
    y = fh[:, s].flatten() / std

    X = pd.Series(x)
    Y = pd.Series(x - y)

    return X, Y


# KDE estimation
def get_kde(X, Y, npoints=npoints, xlim=xlim, ylim=ylim):
    """get kde, return x, y, f"""
    xx, yy = np.mgrid[-xlim:xlim:npoints, -ylim:ylim:npoints]
    positions = np.vstack([xx.ravel(), yy.ravel()])
    values = np.vstack([X, Y])
    kernel = stats.gaussian_kde(values)
    f = np.reshape(kernel(positions).T, xx.shape)

    return xx, yy, f


def plot_kde(
    ds, ax, s=0, cmap="PuBu", npoints=npoints, levels=64, xlim=xlim, ylim=ylim
):
    """plot kde to ax"""
    X, Y = get_xy(ds, s=s)
    xx, yy, f = get_kde(X, Y, npoints=npoints, xlim=xlim, ylim=ylim)

    cnt = ax.contourf(xx, yy, f, cmap=cmap, levels=levels)

    for c in cnt.collections:
        c.set_edgecolor("face")

    ax.axhline(Y.std(), **config.quantiles.kw)
    ax.axhline(-Y.std(), **config.quantiles.kw)

    ax.axvline(X.std(), **config.quantiles.kw)
    ax.axvline(-X.std(), **config.quantiles.kw)

    ax.set_xlim(-xlim, xlim)
    ax.set_ylim(-ylim, ylim)

    ax.set_xticks(np.arange(-xlim, 1.1 * xlim, 0.5))
    ax.set_xticks(np.arange(-xlim, xlim, 0.25), minor=True)

    ax.set_yticks(np.arange(-ylim, 1.1 * ylim, 0.5))
    ax.set_yticks(np.arange(-xlim, xlim, 0.25), minor=True)

    ax.set_aspect(1)

    log.write(f"s = {s}\n")
    log.write(f"X.std() = {X.std()}\n")
    log.write(f"Y.std() = {Y.std()}\n")

    ax.tick_params(direction="in", which="both", right=True, top=True)


plot_kde(ds2, ax1, s=262, cmap=config.cmap.c2)
plot_kde(ds2, ax2, s=90, cmap=config.cmap.c2)

ylabel = r"$p \left( \tilde{F}_{s}^\mathrm{A} \right)$"
plt.text(
    0.53,
    0.53,
    ylabel,
    ha="center",
    va="center",
    transform=fig.transFigure,
    fontsize=fontsize,
)

# Label the System names
x, y = 0.1, 0.9
ha = "left"
va = "top"
fs = fontsize + 2
name1 = r"$\Gamma$, $n=35$"
name2 = r"$\Gamma$, $n=11$"

xlabel = r"$p \left( \tilde{F}_{s} \right)$"
xlabel1 = r"$p \left( \tilde{F}_{s = \Gamma,~ n=35} \right)$"
xlabel2 = r"$p \left( \tilde{F}_{s = \Gamma,~ n=11} \right)$"
ax1.set_xlabel(xlabel)
ax2.set_xlabel(xlabel)

kw = {"ha": ha, "va": va, "fontsize": fs}

ax1.set_title(name1, fontsize=fs)
ax2.set_title(name2, fontsize=fs)

# plt.text(x, y, name1, transform=ax1.transAxes, backgroundcolor="w", **kw)
# plt.text(x, y, name2, transform=ax2.transAxes, backgroundcolor="w", **kw)

fig.savefig("histogram_mode.pdf", **config.save.kw)
