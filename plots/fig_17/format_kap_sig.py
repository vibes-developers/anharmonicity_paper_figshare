import pandas as pd 
import numpy as np 
from ase.atoms import Atoms  
from scipy import stats 
import matplotlib.pyplot as plt 
from pathlib import Path

def prep_data(kappa, sig): 
    inds = np.where(np.isfinite(sig))[0] 
    log_sig = np.log10(sig[inds]) 
    log_kap = np.log10(kappa[inds]) 
 
    p, c, r = stats.linregress(log_sig, log_kap)[:3] 
    pwr_fit = p * log_sig + c 
    afd = 10.0**np.mean(np.abs(pwr_fit - log_kap)) 
     
    x_plt = np.linspace(np.min(log_sig)-np.abs(np.min(log_sig)), np.max(log_sig) + np.abs(np.max(log_sig)), 1001) 
    y_plt = p * x_plt + c 

    return p, c, r**2.0, afd, inds, log_sig, log_kap, 10.0**x_plt, 10.0**y_plt 
 
def save_data_files(df, data, suffix): 
    folder = f"plotting_data/{suffix}"
    Path(folder).mkdir(exist_ok=True, parents=True)
    pwr_fit = np.vstack((data[-2], data[-1])).T 
    np.savetxt(f"{folder}/pwr_fit.dat", pwr_fit) 
     
    cols = [suffix, "kappa_L"] 
    x_1 = 10.0**((1.0 - data[1]) / data[0])
    header = f"slope: {round(data[0], 2)}, R^2: {round(data[2], 2)}, AFD: {round(data[3], 2)}, x_arrow: {x_1}"
    np.savetxt(f"{folder}/kap_sig.dat", df.loc[:, cols].values, header=header)
 
    rs = df.loc[(df["Space_Group"] == 225) & (np.isfinite(df[suffix]))]  
    zb = df.loc[(df["Space_Group"] == 216) & (np.isfinite(df[suffix]))]  
    wz = df.loc[(df["Space_Group"] == 186) & (np.isfinite(df[suffix]))] 
 
    np.savetxt(f"{folder}/rs.dat", rs.loc[:, cols].values) 
    np.savetxt(f"{folder}/zb.dat", zb.loc[:, cols].values) 
    np.savetxt(f"{folder}/wz.dat", wz.loc[:, cols].values) 
     
df = pd.read_csv("kappa_sigma.csv", index_col="Material") 
 
data = prep_data(df.loc[:, "kappa_L"].values, df.loc[:, "sigma_a"].values) 
data_no_nmh = prep_data(df.loc[:, "kappa_L"].values, df.loc[:, "sigma_a_no_nmh"].values)
os_data = prep_data(df.loc[:, "kappa_L"].values, df.loc[:, "sigma_a_os"].values) 
 
save_data_files(df, data, "sigma_a") 
save_data_files(df, os_data, "sigma_a_os") 
save_data_files(df, data_no_nmh, "sigma_a_no_nmh") 

