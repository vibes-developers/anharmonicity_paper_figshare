set terminal cairolatex pdf size 2.9,2.6 standalone color dashed  font 'ptm,10,bx'  dl 2.0 header "\\usepackage {amsmath}\n\\usepackage {color}\n\\definecolor{rred}{RGB}{222,45,38}\n\\definecolor{bblue}{RGB}{27,165,175}"\

set output 'tex_file/kap_sig_log_log.tex'

set border lw 2.4
set tics scale 0.946, 0.540540

set logscale xy
set key t r samplen 0.8
set ylabel "$\\kappa_\\text{300 K}$ (W/mK)"
set format y "10$^\\text{%L}$"
set yrange [0.75:4000]
set xrange [0.075:0.525]

set xtics ("" 0.08 1, "" 0.09 1, "" 0.1, "" 0.11 1, "" 0.12 1, "" 0.13 1, "" 0.14 1, "" 0.15 1, "" 0.16 1, "" 0.17 1, "" 0.18 1, "" 0.19 1, "" 0.2, "" 0.21 1, "" 0.22 1, "" 0.23 1, "" 0.24 1, "" 0.25 1, "" 0.26 1, "" 0.27 1, "" 0.28 1, "" 0.29 1, "" 0.3, "" 0.31 1, "" 0.32 1, "" 0.33 1, "" 0.34 1, "" 0.35 1, "" 0.36 1, "" 0.37 1, "" 0.38 1, "" 0.39 1, "" 0.4, "" 0.41 1, "" 0.42 1, "" 0.43 1, "" 0.44 1, "" 0.45 1, "" 0.46 1, "" 0.47 1, "" 0.48 1, "" 0.49 1, "" 0.5, "" 0.51 1, "" 0.52 1, "" 0.53 1, "" 0.54 1, "" 0.55 1, "" 0.56 1, "" 0.57 1, "" 0.58 1, "" 0.59 1, "" 0.6, "" 0.61 1, "" 0.62 1, "" 0.63 1, "" 0.64 1, "" 0.65 1, "" 0.66 1, "" 0.67 1, "" 0.68 1, "" 0.69 1, "" 0.7, "" 0.71 1, "" 0.72 1, "" 0.73 1, "" 0.74 1, "" 0.75 1, "" 0.76 1, "" 0.77 1, "" 0.78 1, "" 0.79 1, "" 0.8, "" 0.81 1, "" 0.82 1, "" 0.83 1, "" 0.84 1, "" 0.85 1, "" 0.86 1, "" 0.87 1, "" 0.88 1, "" 0.89 1, "" 0.9, "" 0.91 1, "" 0.92 1, "" 0.93 1, "" 0.94 1, "" 0.95 1, "" 0.96 1, "" 0.97 1, "" 0.98 1, "" 0.99 1, "" 1.0)
# set xtics ("" 0.08 1, "" 0.09 1, "" 0.1, "" 0.11 1, "" 0.12 1, "" 0.13 1, "" 0.14 1, "" 0.15 1, "" 0.16 1, "" 0.17 1, "" 0.18 1, "" 0.19 1, "" 0.2, "" 0.21 1, "" 0.22 1, "" 0.23 1, "" 0.24 1, "" 0.25 1, "" 0.26 1, "" 0.27 1, "" 0.28 1, "" 0.29 1, "" 0.3, "" 0.31 1, "" 0.32 1, "" 0.33 1, "" 0.34 1, "" 0.35 1, "" 0.36 1, "" 0.37 1, "" 0.38 1, "" 0.39 1, "" 0.4, "" 0.41 1, "" 0.42 1, "" 0.43 1, "" 0.44 1, "" 0.45 1, "" 0.46 1, "" 0.47 1, "" 0.48 1, "" 0.49 1, "" 0.5, "" 0.6 1, "" 0.7 1, "" 0.8 1, "" 0.9 1, "" 1.0)

set format x "%0.1f"
set xlabel "$\\sigma^\\text{A}$"

set arrow from 0.075,10 to 0.525,10 nohead lw 4 dt (4,3) lc rgb "#aaaaaa"
set arrow from 0.27628576225992424,0.75 to 0.27628576225992424,4000.0 nohead lw 4 dt (4,3) lc rgb "#aaaaaa"

set label at 0.155, 3.4722222222222 right "Slope: -4.79"
set label at 0.155, 2.0833333333333 right "$R^\\text{2}$: 0.93"
set label at 0.155, 1.25 right "AFD: 1.48"

set tics front

set multiplot

plot "plotting_data/sigma_a_no_nmh/pwr_fit.dat" w l lc rgb "#aaaaaa" dt (6, 3) lw 8 notitle, \
     "plotting_data/sigma_a_no_nmh/rs.dat" u 1:2 w p pt 7  lc rgb "#51A351" title "RS", \
     "plotting_data/sigma_a_no_nmh/zb.dat" u 1:2 w p pt 13 lc rgb "#5759AA" title "ZB", \
     "plotting_data/sigma_a_no_nmh/wz.dat" u 1:2 w p pt 15 lc rgb "#82591C" title "WZ"

set border lw 3.2
unset label
unset arrow
set yrange [0.75:4000]
set xrange [0.075:0.525]
set xtics (0.1, 0.2, 0.3, "" 0.4, 0.5, "" 0.6, 0.7, "" 0.8, "" 0.9, 1.0)
set ytics (1, 10, 100, 1000)
unset key

plot NaN
