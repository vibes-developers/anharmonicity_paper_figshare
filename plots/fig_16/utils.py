import numpy as np
import pandas as pd

from vibes.helpers.converters import json2atoms
from vibes.harmonic_analysis.mode_projection import SimpleModeProjection

# from vibes.harmonic_analysis.dynamical_matrix import (
#     get_dynamical_matrices,
#     get_frequencies,
# )
# from vibes.helpers.lattice_points import get_commensurate_q_points


def get_forces_mode(ds):
    """get forces per mode from dataset and return frequencies"""
    supercell = json2atoms(ds.attrs["reference atoms"])
    fc = ds.attrs["flattened force_constants"]

    proj = SimpleModeProjection(supercell, fc)

    x = proj.project(ds.forces, mass_weight=-0.5, info="forces")
    y = proj.project(ds.forces_harmonic, mass_weight=-0.5, info="harmonic forces")
    f = proj.omegas

    return x, y, f


def get_sigma_mode(x, y, relative=False):

    std = x.std()

    sigmas = []
    for n in range(x.shape[1]):
        X = pd.Series((x / std)[:, n].flatten())
        Y = pd.Series(((x - y) / std)[:, n].flatten())

        if relative:
            sigmas.append(Y.std() / X.std())
        else:
            sigmas.append(Y.std())

    return np.array(sigmas)


# def get_fq(ds):
#     primitive = json2atoms(ds.attrs["reference primitive atoms"])
#     supercell = json2atoms(ds.attrs["reference atoms"])
#
#     Na = len(supercell)
#     # masses = supercell.get_masses()
#
#     fc = ds.attrs["flattened force_constants"].reshape(3 * Na, 3 * Na)
#
#     q_points = get_commensurate_q_points(primitive.cell, supercell.cell)
#
#     dynmats = get_dynamical_matrices(q_points, primitive, supercell, fc)
#
#     freqs = []
#     for q, d in zip(q_points, dynmats):
#         freqs.append(get_frequencies(d))  # , masses=primitive.get_masses()))
#
#     qpoints = q_points.repeat(d.shape[0], axis=0)
#
#     sort_args = np.argsort(np.ravel(freqs))
#
#     freqs = np.ravel(freqs)[sort_args]
#     qpoints = qpoints[sort_args]
#
#     return freqs, qpoints

