import numpy as np
import pandas as pd

from plot_config import plt, config, fontsize
from utils import get_forces_mode, get_sigma_mode

import seaborn as sns


def adjacent_values(vals, q1, q3):
    upper_adjacent_value = q3 + (q3 - q1) * 1.5
    upper_adjacent_value = np.clip(upper_adjacent_value, q3, vals[-1])

    lower_adjacent_value = q1 - (q3 - q1) * 1.5
    lower_adjacent_value = np.clip(lower_adjacent_value, vals[0], q1)
    return lower_adjacent_value, upper_adjacent_value


def set_axis_style(ax):
    ax.tick_params(which="both", direction="in")
    ax.set_ylabel("$\sigma^\mathrm{A}_s$")
    ax.set_ylim(0, 1.5)
    yticks = np.arange(0.0, 1.51, 0.25)
    ax.set_yticks(yticks)
    yticks = np.arange(0, 1.51, 0.125)
    ax.set_yticks(yticks, minor=True)
    # ax.set_yscale("symlog", linthreshy=1, linscaley=2.0)
    # ax.set_yscale("log")
    # yticks = np.concatenate((np.arange(0, 1.0, 0.25), np.arange(1, 10.1, 2.5)))

    ytick_labels = [
        "0",
        "0.25",
        "0.5",
        "0.75",
        "1",
        "1.25",
        "1.5",
        "1.75",
        "2.0",
        "2.25",
        "2.5",
    ]
    # ytick_labels = ["0", "0.5", "1", "1.5", "2.0", "2.5"]
    ax.set_yticklabels(ytick_labels, fontsize=10)

    ax.axhline(1, lw=1.0, c="k", alpha=0.75, zorder=0.0, dashes=[4, 4])


files = [
    "CaZrO3_300.dat",
    "CaZrO3_600.dat",
    "NaTaO3_300.dat",
    "NaTaO3_600.dat",
    "KCdF3_300.dat",
    "KCdF3_600.dat",
    "CsSnI3_300.dat",
    "CsSnI3_600.dat",
]

sigs = [np.loadtxt(file, usecols=(1,)) for file in files]
mats = [[file.split("3")[0] + "$_3$"] * len(sigs[ff]) for ff, file in enumerate(files)]
temp = [
    [int(file.split("_")[1].split(".")[0])] * len(sigs[ff])
    for ff, file in enumerate(files)
]

phase_trans = [
    [2023] * len(sigs[0]),
    [2023] * len(sigs[1]),
    [700] * len(sigs[2]),
    [700] * len(sigs[3]),
    [460] * len(sigs[4]),
    [460] * len(sigs[5]),
    [351] * len(sigs[6]),
    [351] * len(sigs[7]),
]

sigs = np.array(sigs).flatten()
mats = np.array(mats).flatten()
temp = np.array(temp).flatten()
phase_trans = np.array(phase_trans).flatten()

df = pd.DataFrame(
    data=np.vstack((sigs, temp, phase_trans)).T,
    columns=["sigma", "Temperature", "Phase Transition"],
)
sigs = [np.loadtxt(file, usecols=(1,)) for file in files]
print(df)
c1 = config.color.c1
c2 = config.color.c2

c1w = config.color.c1w
c2w = config.color.c2w

fig, ax = plt.subplots(nrows=1, ncols=1, figsize=(3.30, 3.25), sharex=True)

# data = [np.loadtxt(file, usecols=(1,)) for file in files[aa::2]]
sns.violinplot(
    x="Phase Transition",
    y="sigma",
    hue="Temperature",
    split=True,
    inner=None,
    palette={300.0: c1, 600.0: c2},
    data=df,
    ax=ax,
)
handles, labels = ax.get_legend_handles_labels()
ax.legend(handles[:2], ["300 K", "600 K"], edgecolor="white")
ax.plot(
    [0, 1, 2, 3],
    [np.median(sigs[6]), np.median(sigs[4]), np.median(sigs[2]), np.median(sigs[0])],
    "o",
    color=c1w,
    ms=3.5,
)
ax.plot(
    [0, 1, 2, 3],
    [np.median(sigs[7]), np.median(sigs[5]), np.median(sigs[3]), np.median(sigs[1])],
    "o",
    color=c2w,
    ms=3.5,
)

set_axis_style(ax)

labels = [
    "CaZrO$_3$\n(2023 K)",
    "NaTaO$_3$\n(720 K)",
    "KCdF$_3$\n(460 K)",
    "CsSnI$_3$\n(351 K)",
]
labels = labels[::-1]

ax.set_xlabel("Material\n(Phase Transition Temperature)")
ax.set_xticklabels(labels)

config.subplots_adjust.kw.update(
    {"left": 0.17, "right": 0.99, "bottom": 0.23, "top": 0.97, "hspace": 0.05}
)

fig.subplots_adjust(**config.subplots_adjust.kw)
fig.savefig("sigma_mode.pdf", **config.save.kw)
