"""inspired by https://www.bastibl.net/publication-quality-plots/"""
import numpy as np
import pandas as pd
import xarray as xr
from matplotlib import font_manager
from scipy import stats

from plot_config import config, fontsize, plt

params = {"text.latex.preamble": [r"\usepackage{amsmath}"]}
plt.rcParams.update(params)

config.subplots_adjust.kw.bottom = 0.2
config.subplots_adjust.kw.left = 0.1
config.subplots_adjust.kw.right = 0.99
config.subplots_adjust.kw.wspace = 0.3
c1 = config.color.c1k
c2 = config.color.c2k

ds1 = xr.load_dataset("../datasets/Si.nc")
ds2 = xr.load_dataset("../datasets/KCaF3.nc")

fig, ((ax1, ax3), (ax2, ax4)) = plt.subplots(ncols=2, nrows=2, sharex="col")
fig.subplots_adjust(**config.subplots_adjust.kw)
fig.set_size_inches(config.size.width, config.size.height)


npoints = 80

xlim1, ylim1 = 2.5, 1.35
xlim2, ylim2 = 2.5, 1.35


def get_x(ds, std=False):
    """return f pd.Series"""
    f = ds.forces.data

    x = f.flatten()

    if std:
        return pd.Series(x / x.std())
    else:
        return pd.Series(x)


# KDE estimation
def get_kde(X, npoints=100, xlim=2):
    """get kde, return x, f"""
    xx = np.linspace(-xlim, xlim, npoints)

    f = stats.gaussian_kde(X)

    return xx, f


def plot_kde(
    ds, ax, c=c1, npoints=npoints, levels=64, xlim=xlim1, ylim=ylim1, std=False
):
    """plot kde to ax"""
    try:
        name = ds.attrs["System Name"]
    except KeyError:
        name = ds.attrs["system_name"]
    print(name)
    X = get_x(ds, std=std)
    xx, f = get_kde(X, npoints=npoints, xlim=xlim)

    ax.plot(xx, f(xx), color=c, alpha=1.0, lw=1.5)
    ax.fill(xx, f(xx), color=c, alpha=0.7)

    ax.set_xlim(-xlim, xlim)
    ax.set_ylim(0, ylim)

    xlim = 2
    ax.set_xticks(np.arange(-xlim, 1.1 * xlim, 1))
    ax.set_xticks(np.arange(-xlim, xlim, 0.5), minor=True)

    ax.set_yticks([0.5], minor=True)
    # ax.set_yticklabels([])

    ax.axvline(X.std(), **config.quantiles.kw)
    ax.axvline(-X.std(), **config.quantiles.kw)

    kw = {"which": "both", "width": 0.75, "zorder": 10}
    ax.tick_params(axis="x", direction="out", **kw)
    ax.tick_params(axis="y", direction="in", **kw)


plot_kde(ds1, ax1, c=c1, xlim=xlim1, ylim=ylim1)
plot_kde(ds2, ax2, c=c2, xlim=xlim1, ylim=ylim1)
plot_kde(ds1, ax3, c=c1, std=True, xlim=xlim2, ylim=ylim2)
plot_kde(ds2, ax4, c=c2, std=True, xlim=xlim2, ylim=ylim2)

# xlabel1 = r"$F_{I, \alpha}$ (eV/$\mathrm{\AA}$)"
# xlabel2 = r"$F_{I, \alpha}$ $/$ $\sigma [F]$ (1)"
xlabel1 = r"$F$ (eV/$\mathrm{\AA}$)"
# xlabel2 = r"$\tilde{F} \equiv F / \sigma [F]$ ~(1)"
xlabel2 = r"$F / \sigma [F]$ ~(1)"
ax2.set_xlabel(xlabel1)
ax4.set_xlabel(xlabel2)

# ylabel = r"$p \left( F_{I, \alpha} \right)$"
pad = 2.5
ylabel1 = r"$p \left( F \right)$"
ax1.set_ylabel(ylabel1, rotation=90, labelpad=pad)
ax2.set_ylabel(ylabel1, rotation=90, labelpad=pad)

# right labels
# ylabel2 = r"$p (\tilde{F}) $"
ylabel2 = r"$p \left(F / \sigma [F] \right)$"
# ax3.yaxis.set_label_position("right")
# ax4.yaxis.set_label_position("right")
ax3.set_ylabel(ylabel2, rotation=90, labelpad=pad)
ax4.set_ylabel(ylabel2, rotation=90, labelpad=pad)
# ax3.set_yticklabels([])
# ax4.set_yticklabels([])

# Label the System names
x, y = 0.98, 0.91
ha = "right"
va = "top"
fs = fontsize + 2
name1 = "Si"
name2 = r"KCaF$_3$"

fprop = font_manager.FontProperties(family="serif")

plt.text(x, y, name1, ha=ha, va=va, transform=ax1.transAxes, fontsize=fs, c=c1)
plt.text(x, y, name2, ha=ha, va=va, transform=ax2.transAxes, fontsize=fs, c=c2)

# figure labels
x = 0.18
plt.text(x, y, "a)", ha=ha, va=va, transform=ax1.transAxes, fontsize=fs, c="k")
plt.text(x, y, "b)", ha=ha, va=va, transform=ax2.transAxes, fontsize=fs, c="k")

x = 0.97
plt.text(x, y, "c)", ha=ha, va=va, transform=ax3.transAxes, fontsize=fs, c="k")
plt.text(x, y, "d)", ha=ha, va=va, transform=ax4.transAxes, fontsize=fs, c="k")


fig.savefig("histogram_forces.pdf", **config.save.kw)
