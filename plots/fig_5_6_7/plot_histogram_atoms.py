"""inspired by https://www.bastibl.net/publication-quality-plots/"""
import numpy as np
import pandas as pd
import xarray as xr
from mpl_toolkits.axes_grid1 import make_axes_locatable

from plot_config import config, fontsize, plt
from utils import get_kde
from vibes.helpers.converters import json2atoms

config.subplots_adjust.kw.update(
    {"left": 0.125, "right": 1.01, "bottom": 0.15, "top": 0.9, "wspace": 0.2}
)
config.size.height = config.size.width / 2.3
config.quantiles.kw.ls = (0, (2, 3))

npoints = 35j
xlim, ylim = 1, 1


log = open("histogram_atoms.log", "w")

ds = xr.load_dataset("../datasets/KCaF3.nc")

try:
    supercell = json2atoms(ds.attrs["atoms_reference"])
except KeyError:
    supercell = json2atoms(ds.attrs["atoms_supercell"])
symbols = np.array(supercell.get_chemical_symbols())
unique_symbols = np.unique(symbols)


dct = {}
for sym in unique_symbols:
    mask = symbols == sym

    f = ds.forces.data[:, mask]
    fh = ds.forces_harmonic.data[:, mask]

    # std = ds.forces.data.std()
    std = f.std()

    x = f / std
    y = (f - fh) / std

    dct[sym] = {"x": x.flatten(), "y": y.flatten(), "norm": std, "sym": f"{sym}"}

    log.write(f"sym = {sym}\n")
    log.write(f"x.std() = {x.std()}\n")
    log.write(f"y.std() = {y.std()}\n\n")


fig, (ax1, ax2, ax3) = plt.subplots(ncols=3, sharey=True)
fig.subplots_adjust(**config.subplots_adjust.kw)
fig.set_size_inches(config.size.width, config.size.height)


def plot_kde(d, ax, cmap="OrRd", npoints=npoints, levels=64, xlim=xlim, ylim=ylim):
    """plot kde to ax"""
    x, y = d["x"], d["y"]
    std = d["norm"]  # * x.std()

    X, Y = pd.Series(x), pd.Series(y)

    xx, yy, f = get_kde(X, Y, npoints=npoints, xlim=xlim, ylim=ylim)

    cnt = ax.contourf(xx, yy, f, cmap=cmap, levels=levels)

    for c in cnt.collections:
        c.set_edgecolor("face")

    ax.axhline(Y.std(), **config.quantiles.kw)
    ax.axhline(-Y.std(), **config.quantiles.kw)
    # ax.axvline(X.std(), **config.quantiles.kw)
    # ax.axvline(-X.std(), **config.quantiles.kw)

    ax.set_xlim(-xlim, xlim)
    ax.set_ylim(-ylim, ylim)

    ax.set_xticks(np.arange(-1, 1.1, 1))
    ax.set_xticks(np.arange(-xlim, xlim, 0.5), minor=True)

    ax.set_yticks(np.arange(-1, 1.1, 1))
    ax.set_yticks(np.arange(-xlim, xlim, 0.5), minor=True)

    ax.set_aspect(1)
    print(Y.std())

    ax.tick_params(direction="in", which="both", right=True, top=True, width=0.75)

    return cnt


plot_kde(dct["K"], ax1, cmap=config.cmap.c2)
plot_kde(dct["Ca"], ax2, cmap=config.cmap.c2)
cnt = plot_kde(dct["F"], ax3, cmap=config.cmap.c2)

xlabel = r"$p \left( \tilde{F}_{\alpha} \right)$"
xlabel = r"$ {F}_{\alpha} \right)$"

ax1.set_xlabel(r"$F_\mathrm{K} / \sigma[F_\mathrm{K}]$")
ax2.set_xlabel(r"$F_\mathrm{Ca} / \sigma[F_\mathrm{Ca}]$")
ax3.set_xlabel(r"$F_\mathrm{F} / \sigma[F_\mathrm{F}]$")

ylabel = r"$p \left( \tilde{F}_{\alpha}^\mathrm{A} \right)$"
ylabel = r"$F_I^\mathrm{A}  / \sigma[F_I]$"
ax1.set_ylabel(ylabel, labelpad=-4, y=0.6)

# Label the System names
x, y = 0.1, 0.9
ha = "left"
va = "top"
fs = fontsize + 2
name1 = "Si"
name2 = r"KCaF$_3$"

ax1.set_title("K", fontsize=fs)
ax2.set_title("Ca", fontsize=fs)
ax3.set_title("F", fontsize=fs)

# colorbar
# cb_ax = fig.add_axes([0.93, 0.1, 0.02, 0.5])
# cbar = fig.colorbar(cnt, cax=cb_ax)
cbar = fig.colorbar(cnt, ax=[ax1, ax2, ax3], shrink=0.65, pad=0.02)
divider = make_axes_locatable(ax3)
# cax = divider.append_axes("right", size="5%", pad=0.05)
#
# cbar = fig.colorbar(cnt, cax=cax, orientation="vertical")
cbar.set_label("Probability density", rotation=-90, labelpad=15)
cbar.set_ticks([])

fig.savefig("histogram_atoms.pdf", **config.save.kw)
