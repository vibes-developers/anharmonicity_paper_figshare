"""inspired by https://www.bastibl.net/publication-quality-plots/"""
import numpy as np
import xarray as xr
from mpl_toolkits.axes_grid1 import make_axes_locatable

from plot_config import config, fontsize, plt
from utils import get_kde, get_xy

# adjust height
config.size.height *= 0.9
config.subplots_adjust.kw.update({"left": 0.16, "right": 0.9, "wspace": 0.125})

log = open("histogram.log", "w")


ds1 = xr.load_dataset("../datasets/Si.nc")
ds2 = xr.load_dataset("../datasets/KCaF3.nc")


fig, (ax1, ax2) = plt.subplots(ncols=2, sharey=True)
fig.subplots_adjust(**config.subplots_adjust.kw)
fig.set_size_inches(config.size.width, config.size.height)


npoints = 35j
xlim, ylim = 1, 1


def plot_kde(
    ds, ax, fig, cmap="PuBu", npoints=npoints, levels=64, xlim=xlim, ylim=ylim
):
    """plot kde to ax"""
    try:
        name = ds.attrs["System Name"]
    except KeyError:
        name = ds.attrs["system_name"]
    print(name)
    X, Y, std = get_xy(ds)
    xx, yy, f = get_kde(X, Y, npoints=npoints, xlim=xlim, ylim=ylim)

    cnt = ax.contourf(xx, yy, f, cmap=cmap, levels=levels)

    for c in cnt.collections:
        c.set_edgecolor("face")

    ax.axhline(Y.std(), **config.quantiles.kw)
    ax.axhline(-Y.std(), **config.quantiles.kw)

    ax.set_xlim(-xlim, xlim)
    ax.set_ylim(-ylim, ylim)

    ax.set_xticks(np.arange(-xlim, 1.1 * xlim, 0.5))
    ax.set_xticks(np.arange(-xlim, xlim, 0.25), minor=True)

    ax.set_yticks(np.arange(-ylim, 1.1 * ylim, 0.5))
    ax.set_yticks(np.arange(-xlim, xlim, 0.25), minor=True)

    ax.set_aspect(1)
    print(Y.std())

    log.write(f"System: {name}\n")
    log.write(f"F.std = {std}\n")
    log.write(f"X.std = {X.std()}\n")
    log.write(f"Y.std = {Y.std()}\n\n")

    ax.tick_params(direction="in", which="both", right=True, top=True)
    ax.tick_params(which="both", width=0.75)

    divider = make_axes_locatable(ax)
    cax = divider.append_axes("right", size="5%", pad=0.05)

    cbar = fig.colorbar(cnt, cax=cax, orientation="vertical")

    # ylabel = r"$p \left( \tilde{F}_{I, \alpha}^\mathrm{A} \right)$"
    # cbar.set_label(ylabel)
    cbar.set_ticks([])

    return cbar


cbar = plot_kde(ds1, ax1, fig, cmap=config.cmap.c1)
cbar = plot_kde(ds2, ax2, fig, cmap=config.cmap.c2)

xlabel = r"$ \tilde{F}_{I, \alpha}$"
for ax in (ax1, ax2):
    ax.set_xlabel(xlabel)
# ax1.set_xlabel(r"$F / \sigma_\mathrm{Si}[F]$")
# ax2.set_xlabel(r"$F / \sigma_\mathrm{KCaF}[F]$")

# cbar label
# ylabel = r"$p \left( \tilde{F}_{I, \alpha}^\mathrm{A} \right)$"
# ylabel = r"$\tilde{F}_{I, \alpha}^\mathrm{A}$"
# ylabel = r"$F_{I, \alpha}^\mathrm{A}$ $/$ $\sigma [F]$"
ylabel = r"$F^\mathrm{A}  / \sigma[F]$"
cbar.set_label("Probability density", rotation=-90, labelpad=15)

kw = {
    "ha": "center",
    "va": "center",
    "transform": fig.transFigure,
    "fontsize": fontsize,
}
# plt.text(0.53, 0.53, ylabel, **kw)
ax1.set_ylabel(ylabel, rotation=90, labelpad=0)

# Label the System names
x, y = 0.1, 0.9
ha = "left"
va = "top"
fs = fontsize + 2
name1 = "Si"
name2 = r"KCaF$_3$"

ax1.set_title(name1, fontsize=fs)
ax2.set_title(name2, fontsize=fs)


# plt.text(x, y, name1, ha=ha, va=va, transform=ax1.transAxes, fontsize=fs)
# plt.text(x, y, name2, ha=ha, va=va, transform=ax2.transAxes, fontsize=fs)

fig.savefig("histogram.pdf", **config.save.kw)
