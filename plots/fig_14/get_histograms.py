import numpy as np
import matplotlib.pyplot as plt
import os

data = np.genfromtxt(
    "sigma.csv",
    delimiter=",",
    filling_values=np.nan,
    usecols=(2, 5, 6, 7, 8, 9, 10, 11, 12),
)

data_rs = data[np.where(data[:, 0] == 225.0)[0], 1:]
data_zb = data[np.where((data[:, 0] == 216.0) | (data[:, 0] == 227.0))[0], 1:]
data_wz = data[np.where(data[:, 0] == 186.0)[0], 1:]

bin_size = 0.1
min_edge = 0
max_edge = 1.0

N = int((max_edge - min_edge) / bin_size)

bin_edge = np.linspace(min_edge, max_edge, N + 1)
bin_center = np.linspace(min_edge + bin_size / 2.0, max_edge - bin_size / 2.0, N)

for ii, temp in enumerate(range(300, 1001, 100)):
    plt.clf()
    hist_tot = np.array(
        [
            bin_center,
            np.ones(shape=bin_center.shape) * data.shape[0],
            plt.hist(data[:, ii + 1], bins=bin_edge)[0],
        ]
    )
    hist_rs = np.array(
        [
            bin_center,
            np.ones(shape=bin_center.shape) * data_rs.shape[0],
            plt.hist(data_rs[:, ii], bins=bin_edge)[0],
        ]
    )
    hist_zb = np.array(
        [
            bin_center,
            np.ones(shape=bin_center.shape) * data_zb.shape[0],
            plt.hist(data_zb[:, ii], bins=bin_edge)[0],
        ]
    )
    hist_wz = np.array(
        [
            bin_center,
            np.ones(shape=bin_center.shape) * data_wz.shape[0],
            plt.hist(data_wz[:, ii], bins=bin_edge)[0],
        ]
    )
    #   hist_tot = np.array(
    #       [
    #           bin_center,
    #           np.ones(shape=bin_center.shape)*data.shape[0],
    #           plt.hist(data[:,ii+1], bins=bin_edge)[0],
    #       ]
    #   )
    np.savetxt(f"plotting_data/total/histogram_{temp}.dat", hist_tot.T)
    np.savetxt(f"plotting_data/rs/histogram_{temp}.dat", hist_rs.T)
    np.savetxt(f"plotting_data/zb/histogram_{temp}.dat", hist_zb.T)
    np.savetxt(f"plotting_data/wz/histogram_{temp}.dat", hist_wz.T)

#    plt.show()
