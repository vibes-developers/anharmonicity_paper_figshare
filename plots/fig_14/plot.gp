set terminal cairolatex pdf size 8.5cm,3.50cm standalone color dashed  font 'ptm,10,bx'  dl 2.0 header "\\usepackage {amsmath}\n\\usepackage {color}\n\\definecolor{rred}{RGB}{222,45,38}\n\\definecolor{bblue}{RGB}{27,165,175}"

set output 'tex_file/histograms.tex'

set linetype 9 lw 2.5 lc rgb "#5555555"
set linetype 10 lw 2 lc rgb "#7777777" dt (4,4)

y_axis_label_space = 0.125581
x_size = ( 1.0 - y_axis_label_space ) / 2.0

# set style textbox border lc black lw 2
set style fill solid 1.0 border 8
set boxwidth 0.8 relative

set border lw 3.2
set tics scale 0.946, 0.540540

set rmargin 1.2
set tmargin 0.6
set lmargin 6.0
set bmargin 3.2

unset key
unset colorbox

set yrange [0.0:45]
set ytics 15
unset mytics
# set mytics 2

set xrange [0.00:1.00]
set xtics 0.1,0.4,0.9
unset mxtics
# set mxtics 8

set multiplot

set size x_size + y_axis_label_space, 1.0
set origin 0.0, 0.0

set ylabel "\\% Material"
set format y "%2.f"

set xlabel "$\\sigma^\\mathrm{A}_\\mathrm{OS}$"
set format x "%1.1f"

set label front right at 0.975, 37.5 "All, 300 K"
set label front left at 0.03, 37.5 "g)"

set tics front
set grid front ytics ls 10
set style fill transparent solid 0.5 noborder

plot NaN

unset label

set border lw 2.4
set mytics 2
set mxtics 4

set tics back
unset grid
set style fill solid 1.0 border 8
plot "plotting_data/total/histogram_300.dat" u ($1):(100.0*$3/$2) with boxes lt 2 lc rgb "#895B96" notitle

set size x_size, 1.0
set origin x_size + y_axis_label_space, 0.0

set xlabel "$\\sigma^\\mathrm{A}_\\mathrm{OS}$"
set format x "%1.1f"

unset ylabel
set format y ""
set lmargin 0.0

set border lw 3.2
unset mxtics
unset mytics

set label front right at 0.975, 37.5 "All, 500 K"
set label front left at 0.03, 37.5 "h)"

set tics front
set grid front ytics ls 10
set style fill transparent solid 0.5 noborder

plot NaN

set border lw 2.4
set mytics 2
set mxtics 4

unset label

set tics back
unset grid
set style fill solid 1.0 border 8
plot "plotting_data/total/histogram_500.dat" u ($1):(100.0*$3/$2) with boxes lt 2 lc rgb "#895B96" notitle

unset multiplot
