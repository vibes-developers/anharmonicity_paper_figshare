set terminal cairolatex pdf size 3.0,2.7 standalone color dashed  font 'ptm,10,bx'  dl 2.0 header "\\usepackage {amsmath}\n\\usepackage {color}\n\\definecolor{rred}{RGB}{222,45,38}\n\\definecolor{bblue}{RGB}{27,165,175}"\

set output 'tex_file/md_os_plot.tex'
set border lw 2.4
set tics scale 0.946, 0.540540

# set mytics 2
# set mxtics 2

set xlabel "$\\sigma^\\text{A}_\\text{OS}$"
set xrange [0.05:1.15]
set xtics (0.1, 0.2, 0.3, 0.4 1, 0.5, 0.6 1, 0.7 1, 0.8 1, 0.9 1, 1.0)
# set format x "10$^\\text{%L}$"
set format y "%0.1f"
# set ytics 0.2

set ylabel "$\\sigma^\\text{A}_\\text{MD}$"
set yrange [0.05:3.05]
set ytics (0.1, 0.2 , 0.3 , 0.4 1, 0.5, 0.6 1, 0.7 1, 0.8 1, 0.9 1, 1.0, 1.1 1, 1.2 1, 1.3 1, 1.4 1, 1.5 1, 1.6 1, 1.7 1, 1.8 1, 1.9 1, 2.0, 2.1 1, 2.2 1, 2.3 1, 2.4 1, 2.5 1, 2.6 1, 2.7 1, 2.8 1, 2.9 1, 3.0)
# set format y "10$^\\text{%L}$"
set format x "%0.1f"
# set xtics 0.5

set arrow from 0.20, 0.05 to 0.20,3.05 lc rgb "#aaaaaa" dt (4, 2) lw 8 nohead back
set arrow from 0.30, 0.05 to 0.30,3.05 lc rgb "#aaaaaa" dt (4, 2) lw 8 nohead back
set arrow from 0.40, 0.05 to 0.40,3.05 lc rgb "#aaaaaa" dt (4, 2) lw 8 nohead back
set arrow from 0.50, 0.05 to 0.50,3.05 lc rgb "#aaaaaa" dt (4, 2) lw 8 nohead back
set arrow from 1.00, 0.05 to 1.0,3.05 lc rgb "#aaaaaa" dt (4, 2) lw 8 nohead back

set arrow from 0.05, 0.6561 to 0.2, 0.6561 lc rgb "#aaaaaa" lw 6 filled back
set arrow from 0.05, 0.7290 to 0.3, 0.7290 lc rgb "#aaaaaa" lw 6 filled back
set arrow from 0.05, 0.8100 to 0.4, 0.8100 lc rgb "#aaaaaa" lw 6 filled back
set arrow from 0.05, 0.9000 to 0.5, 0.9000 lc rgb "#aaaaaa" lw 6 filled back
set arrow from 0.05, 1.0000 to 1.0, 1.0000 lc rgb "#aaaaaa" lw 6 filled back
# set arrow from 0.40, 0.08 to 0.40,0.40 lc rgb "#aaaaaa" dt (4, 2) lw 8 nohead back

set label at 0.185, 0.05125 rotate by 90 left "\\footnotesize{RMSE: 0.012}"
set label at 0.275, 0.05125 rotate by 90 left "\\footnotesize{RMSE: 0.023}"
set label at 0.365, 0.05125 rotate by 90 left "\\footnotesize{RMSE: 0.030}"
set label at 0.46,  0.05125 rotate by 90 left "\\footnotesize{RMSE: 0.091}"
set label at 0.9,   0.05125 rotate by 90 left "\\footnotesize{RMSE: 0.426}"
set logscale xy
set tics front

set multiplot
# set key b r samplen 0.8
set key t l samplen 0.8

set origin 0.0, 0.0
set size 1.0, 1.0

# plot 1.2*x with filledcurve x lc rgb"#eeeecc" lw 0 notitle, \
#      0.8*x with filledcurve x lc rgb"#ffffff" lw 0 notitle, \
#      x  w l lc rgb "#aaaaaa" dt (6, 3) lw 8 notitle, \
#      "plotting_data/zb.dat" u 2:1 w p pt 13 ps 0.75 lw 0 lc rgb "#5759AA" title "ZB", \
#      "plotting_data/rs.dat" u 2:1 w p pt 7  ps 0.75 lw 0 lc rgb "#51A351" title "RS", \
#      "plotting_data/wz.dat" u 2:1 w p pt 15 ps 0.75 lw 0 lc rgb "#82591C" title "WZ", \
#      "plotting_data/perov.dat" u 2:1 w p pt 9 ps 0.75 lw 0 lc rgb "#1C3144" title "Perov."

plot x  w l lc rgb "#aaaaaa" dt (6, 3) lw 8 notitle, \
     "plotting_data/zb.dat" u 2:1 w p pt 13 ps 0.75 lw 0 lc rgb "#5759AA" title "ZB", \
     "plotting_data/rs.dat" u 2:1 w p pt 7  ps 0.75 lw 0 lc rgb "#51A351" title "RS", \
     "plotting_data/wz.dat" u 2:1 w p pt 15 ps 0.75 lw 0 lc rgb "#82591C" title "WZ", \
     "plotting_data/perov.dat" u 2:1 w p pt 9 ps 0.75 lw 0 lc rgb "#1C3144" title "Perov."

set border lw 3.2
unset arrow
unset label
unset mxtics
unset mytics
unset key

plot NaN

# set origin 0.15, 0.52
# set size 0.5, 0.45
# unset logscale
# set mytics 2
# set mxtics 2

# set ylabel "$\\sigma^\\text{A}_{OS}$"
# set yrange [0.08:0.35]
# set format y "%0.1f"
# set ytics 0.1

# set xlabel "$\\sigma^\\text{A}_{MD}$"
# set xrange [0.08:0.35]
# set format x "%0.1f"
# set xtics 0.1

# set label at 0.325, 0.11 right "RMSE: 0.02"

# set tics front

# unset key

# plot x  w l lc rgb "#aaaaaa" dt (6, 3) lw 8 notitle, \
#      "plotting_data/zb.dat" w p pt 13 ps 1.0 lw 0 lc rgb "#5759AA" title "ZB", \
#      "plotting_data/rs.dat" w p pt 7  ps 1.0 lw 0 lc rgb "#51A351" title "RS", \
#      "plotting_data/wz.dat" w p pt 15 ps 1.0 lw 0 lc rgb "#82591C" title "WZ", \
#      "plotting_data/perov.dat" w p pt 9 ps 1.0 lw 0 lc rgb "#1C3144" title "pnma Perovskite"

# set border lw 3.2
# unset label
# unset mxtics
# unset mytics
# unset key

# plot NaN

unset multiplot

