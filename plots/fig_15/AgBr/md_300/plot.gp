set terminal cairolatex pdf size 3.5,2.5 standalone color dashed  font 'ptm,10,bx'  dl 2.0 header "\\usepackage {amsmath}\n\\usepackage {color}\n\\definecolor{rred}{RGB}{222,45,38}\n\\definecolor{bblue}{RGB}{27,165,175}"\

set output 'sigma_nmh_md.tex'

set border lw 2.4
set tics scale 0.946, 0.540540

unset key
set mxtics 2
set mytics 2
set ylabel "$\\sigma^\\mathrm{A}\\left[\\mathbf{R}\\left(t\\right)\\right]$"
set format y "%.1f"
set yrange [0.5:5.0]
set ytics 1.0

set xrange [0:10]
set xlabel "time (ps)"
set xtics 1
set format x "%0.0f"

set tics front

set multiplot

plot "sigma.dat" u ($1/1000):2 w l lc rgb "#5759aa" lw 4 notitle 

set border lw 3.2
unset mxtics
unset mytics
unset key

plot NaN
