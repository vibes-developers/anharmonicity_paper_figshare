import numpy as np 
 
from ase.data import atomic_numbers, chemical_symbols 
from ase.dft.kpoints import get_cellinfo 
from ase.io.aims import read_aims 
 
from vibes.trajectory import reader 
from vibes.phonopy.wrapper import preprocess, plot_bandstructure 
from vibes.phonopy.utils import remap_force_constants 
from vibes.helpers.converters import dict2atoms 
from vibes.spglib.wrapper import standardize_cell, get_symmetry_dataset 
 
trajectory = reader(file="trajectory.son") 
trajectory.set_force_constants_remapped( 
    np.loadtxt("phonopy_output/FORCE_CONSTANTS_remapped") 
) 

np.savetxt("sigma.dat", np.column_stack((trajectory.times, trajectory.sigma_per_sample)))
trajectory[620:670].average_atoms.write("geometry.in.average.defect", format="aims", scaled=True) 
trajectory[1450:1775].average_atoms.write("geometry.in.average.prist", format="aims", scaled=True)
trajectory[1450:1500].average_atoms.write("geometry.in.average.prist.1", format="aims", scaled=True)
trajectory[1490:1540].average_atoms.write("geometry.in.average.prist.2", format="aims", scaled=True)
trajectory[1500:1550].average_atoms.write("geometry.in.average.prist.3", format="aims", scaled=True)

