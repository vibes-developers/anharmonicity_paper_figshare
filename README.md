# Anharmonicity Measure for Materials: Figures

This repository contains the data and scripts to create the plots in 

```
@article{Knoop2020,
  author       = {Knoop, Florian and Purcell, Thomas A.R. and Scheffler, Matthias and Carbogno, Christian},
  eid          = {arXiv:2006.14672},
  eprint       = {2006.14672},
  eprintclass  = {cond-mat},
  eprinttype   = {arXiv},
  journaltitle = {arXiv e-prints},
  pages        = {arXiv:2006.14672},
  title        = {Anharmonicity Measure for Materials},
  year         = {2020},
}
```

